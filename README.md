## Описание
Ansible код пулящий и запускающий docker образ. <br />
Для использования с другим сервисом нужно заменить образ docker на свой в ansible/roles/start_app_in_docker/vars/main.yml

## Использование
```
cd ansible
ansible-playbook docker_container_playbook.yml
```

## Runtime
Приложение отвечает по 3 эндпоинтам:  
* /health - 200 ok
* /metrics - в формате метрик для prometheus, включая счётчик запросов в основной эндпоинт `skillbox_http_requests_total`
* / - основной эндпоинт, возвращающий часть запроса и генерирующий строчку лога.

## Изменение конфигурации 
* ansible/roles/start_app_in_docker/vars/main.yml - файл с переменными для настройки
    * container_image: _imagename:tag_ - имя docker образа на dockerhub или путь к образу. При отсутствии tag используется latest
      
